﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public int Co = 0;
    public GameObject hpGauge;
    public GameObject ArrowCount = null;


    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge");
    }

    public void DecreaseHp()
    {
        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;
    }

    public void DecreaseCo()
    {
        Co++;
    }

    void Update()
    {
        if(this.hpGauge.GetComponent<Image>().fillAmount == 0)
            SceneManager.LoadScene("GameOverScene");

        Text ArrowText = ArrowCount.GetComponent<Text>();
        ArrowText.text = "SCORE:" + Co;
    }
}
