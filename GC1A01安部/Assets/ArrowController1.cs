﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController1 : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("player");

    }

    // Update is called once per frame
    void Update()
    {
        //フレームごとに等速で落下させる
        transform.Translate(-0.15f, 0, 0);

        //画面外に出たらオブジェクトを破棄する
        if (transform.position.x < -11.5f)
        {
            Destroy(gameObject);
        }

        Vector2 p1 = transform.position;
        Vector2 p2 = this.player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.25f;
        float r2 = 0.5f;

        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseCo();

            Destroy(gameObject);
        }
    }
}
