﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedArrowGenerator : MonoBehaviour
{

    public GameObject RedArrowPrefab;
    float span = 0.5f;
    float delta = 0;


    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(RedArrowPrefab) as GameObject;
            float py = Random.Range(2.10f, -4.6f);
            go.transform.position = new Vector3(5.25f, py, 0);
        }
    }
}
